# Appium cross platform-Digit88
An Appium (java) demo project for cross platform testing of an Android and an iOS application

Instructions:

1. Download and setup Appium (see http://appium.io/getting-started.html)

2. Import this project as a maven project in IDE. E.g. IntelliJ or Eclipse.

3. Set the executions variable in the AppiumController class (src/test/java/AppiumSupport/AppiumController.java) to Android or iOS, depending on what platform you want to execute the example test on. (For Android, it should work to run on both emulator and device but for iOS, it will only work on the simulator.) Since I do not have an iOS device, hence I could not add the xpaths. However, I have made a class named RandomDetailPageIOS, where you can add the xpaths in order to run the test in iOS devices.

4. Start the appium server and run the demo test 'Random Search' in src/test/java/Test/Test_RandomSearch.java.

5. I used 'Page Object Model Approach' to implement the framework.

6. You need to add .ipa file in app folder for the ios testing.
