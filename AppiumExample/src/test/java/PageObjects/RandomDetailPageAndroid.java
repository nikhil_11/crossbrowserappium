package PageObjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.junit.Assert;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RandomDetailPageAndroid implements RandomDetailPage {

    public RandomDetailPageAndroid(AppiumDriver driver) {
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

   
    
    @FindBy(id = "com.vasilchenko.randomfree:id/number")
    private MobileElement GenerateNumber;

    @FindBy(id = "com.vasilchenko.randomfree:id/person")
    private MobileElement SelectPerson;
    
    @FindBy(id = "com.vasilchenko.randomfree:id/password")
    private MobileElement GeneratePassword;

    
    public void assertGenerateNumber(String expectedResult) {
        Assert.assertEquals(expectedResult, GenerateNumber.getText().toUpperCase());
        System.out.println(GenerateNumber.getText());
    }
    public void navigateToGenerateNumber() {
    	GenerateNumber.click();
    }
    public void assertSelectPerson(String expectedResult) {
        Assert.assertEquals(expectedResult, SelectPerson.getText().toUpperCase());
        System.out.println(SelectPerson.getText());
    }
    public void navigateToSelectPerson() {
    	SelectPerson.click();
    }
    public void assertGeneratePassword(String expectedResult) {
        Assert.assertEquals(expectedResult, GeneratePassword.getText().toUpperCase());
        System.out.println(GeneratePassword.getText());
    }
    public void navigateToGeneratePassword() {
    	GeneratePassword.click();
    }
	
	
}
