package PageObjects;

public interface RandomDetailPage {

    void assertGenerateNumber(String expectedResult);
    void navigateToGenerateNumber();
    void assertSelectPerson(String expectedResult);
    void navigateToSelectPerson();
    void assertGeneratePassword(String expectedResult);
    void navigateToGeneratePassword();
}
